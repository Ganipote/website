+++
title = "Документация"
+++

## Начало работы

Мы настоятельно рекомендуем вам начать с [Книги](https://doc.redox-os.org/book/), поскольку в ней описывается, как настроить и использовать Redox.

## Ссылки

### Cargo Docs

- [redox_syscall](https://docs.rs/redox_syscall/latest/syscall/) - Документация по системным вызовам Redox.

- [libstd](https://doc.rust-lang.org/stable/std/) — Документация для стандартной библиотеки Rust.

### Gitlab

- [Gitlab](https://gitlab.redox-os.org/) - Репозиторий Redox OS Gitlab, где вы можете найти весь исходный код.

- [Drivers](https://gitlab.redox-os.org/redox-os/drivers/-/blob/master/README.md) - Высокий уровень документации драйверов.

- [RFCs](https://gitlab.redox-os.org/redox-os/rfcs) - Запрос на изменения в Redox.

- [Ion Manual](https://doc.redox-os.org/ion-manual/) - Документация для Ion.

- [Talks](/talks/) - Доклады Redox на различных мероприятиях и конференциях.

## Сотрудничество с Redox

- Прочтите [CONTRIBUTING.md](https://gitlab.redox-os.org/redox-os/redox/-/blob/master/CONTRIBUTING.md)
