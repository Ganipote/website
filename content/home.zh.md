+++
title = "Home"
url = "home"
+++

<!--Two-Space indentation is necessary here 🤮🤮 -->

<div id="sponsors">
  <h1>赞助商</h1>
  <p>没有赞助商的慷慨和持续支持, 继续开发Redox OS是非常具有挑战性的. 我们非常感谢下面提到的每个赞助商. </p>
  
  <div class="row">
    <p>还没有赞助商 <a href="/donate">成为第一个!</a></p>
  </div>
  
  <a href="./donate"><button class="donate">捐赠</button></a>
  
  <h2>个人赞助商</h2>
  <p>这个名单当然也包括向Redox OS项目捐赠的个人. </p>
</div>

<hr />

<div>
  <h1>什么是Redox</h1>
  
  <p>Redox是一个使用Rust编写的免费、开源的操作系统. </p>
  <p>采用微内核设计, 使我们能够专注于安全、安全性和稳定性. </p>
  
  <a href="#learn-more"><button class="external">了解更多</button></a>
  <a href="https://gitlab.redox-os.org/redox-os/redox/-/blob/master/CONTRIBUTING.md"><button class="gitlab">加入进来!</button></a>
  <a href="/donate"><button class="donate">捐赠</button></a>
</div>

<div class="advantage">
  <h1 id="learn-more">Rust保护加持</h1>
  <h2>有效消除 Bugs</h2>
  <p>Rust的编译时安全性和限制性语法有效消除了内存错误. </p>

  <div class="card">
    <h2>更加抗数据损坏</h2>
    <article>
      <p>Rust编译器几乎不可能损坏内存, 使Redox更加可靠和稳定. </p>
    </article>
  </div>
  
  <h2>更好的安全性, 更好的安全性</h2>
  <p>内存安全问题可以占据70%的漏洞<sup>2</sup>. Rust防止了这些问题, 使Redox从设计上更安全. </p>
  
  <div class="card">
    <h2>保持多任务安全</h2>
    <article>
      <p>
        C/C++在同时运行多个任务时可能会遇到数据问题. 这可能会导致隐蔽的问题或危险. Rust通过在数据被使用或更改之前验证数据来避免这些问题. 
      </p>
    </article>
  </div>
</div>

<div class="advantage">
  <h1>微内核架构</h1>
  <div class="card">
    <h2>减少权限更安全</h2>
    <article>
      <p>
        微内核是提供实现操作系统所需机制的几乎最小数量的软件, 这些机制在处理器的最高权限下运行. 
      </p>
    </article>
  </div>
  
  <div class="card">
    <h2>通过缺陷隔离实现稳定性</h2>
    <article>
      <p>
        在用户空间运行的系统组件与系统内核隔离. 因此, 大多数错误不会影响和使整个系统崩溃. 
      </p>
    </article>
  </div>
  
  <div class="card">
    <h2>模块化和无需重启的设计</h2>
    <article>
      <p>
        稳定的微内核变化少, 重新启动很少. Redox中的大多数系统部分在用户空间, 因此可以在不需要系统重新启动的情况下替换它们. 
      </p>
    </article>
  </div>
  
  <div class="card">
    <h2>易于开发和调试</h2>
    <article>
      <p>Redox使软件开发和改进更加容易. 系统部分与微内核分开, 简化了测试和调试. </p>
    </article>
  </div>
</div>

<div class="advantage">
  <h1>在Redox上运行Linux应用程序</h1>
  <div class="card">
    <h2>Relibc：Redox OS的Linux桥接器</h2>
    <article>
      <p>Relibc是一种特殊的库, 使Redox OS能够运行许多Linux应用程序. 它充当Linux应用程序和Redox OS语言之间的桥梁. </p>
    </article>
  </div>
  
  <div class="card">
    <h2>与Linux应用程序兼容性不断增长</h2>
    <article>
      <p>目前, 数十个程序和更多的库可以轻松工作. 当前正在将超过1000个程序和库移植到Redox. </p>
    </article>
  </div>
</div>

<div class="advantage">
  <h1>开源和免费</h1>
  
  <div class="card">
    <h2>自由运行任何软件</h2>
    <article>
      <p>使用Redox OS并运行任何兼容的软件, 无论任何目的, 都不用担心源代码或许可问题. </p>
    </article>
  </div>

  <div class="card">
    <h2>修复错误或报告它们</h2>
    <article>
      <p>任何人都可以检查Redox源代码的质量和安全性, 改进它, 或向开发人员报告错误问题. </p>
    </article>
  </div>
  
  <div class="card">
    <h2>永久自由</h2>
    <article>
      <p>没有公司拥有或控制Redox OS. Redox OS是一个由资助和捐款支持的非营利项目. </p>
    </article>
  </div>
</div>

<!-- Latest News -->

<hr />

<h1>最新消息</h1>

<p>目前还没有内容</p>